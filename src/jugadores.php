
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Liga Española</title>
	<link rel="icon" href="img/core-img/balon.png">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="preloader">
        <div class="preload-content">
            <div id="original-load"></div>
        </div>
    </div>

   

    <header class="header-area">


        <div class="top-header">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 col-sm-8">
                        <div class="breaking-news-area">
                            <div id="breakingNewsTicker" class="ticker">
                                <ul>
                                    <li><a href="#">Hello World!</a></li>
                                    <li><a href="#">Hello Universe!</a></li>
                                    <li><a href="#">Hello Original!</a></li>
                                    <li><a href="#">Hello Earth!</a></li>
                                    <li><a href="#">Hello Colorlib!</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="top-social-area">
                            <a href="https://www.facebook.com/LaLiga" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/LaLiga?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/laliga/?hl=es" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://www.linkedin.com/company/laliga" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
							<a href="https://www.youtube.com/user/laliga" data-toggle="tooltip" data-placement="bottom" title="YouTube"><i class="fa fa-youtube" aria-hidden="true"></i></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="logo-area text-center">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <a href="index.php" class="original-logo"><img src="img/core-img/logotipo1.png" width="566"alt=""></a>
                    </div>
                </div>
            </div>
        </div>

           <div class="original-nav-area" id="stickyNav">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <nav class="classy-navbar justify-content-between">
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>
						<div class="navdar">

                        <div class="classy-menu" id="originalNav">
                           <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <div class="classynav">

                                <ul>
                                    <li><a href="index.php">Inicio</a></li>
                                    <li><a href="equipos.php">Equipos</a>
                                        <ul class="dropdown">
                                            <li><a href="jugadores.php">Jugadores</a></li>
                                            <li><a href="fichajes.php">Fichajes</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="partidos.php">Jugadores con mas partidos</a>
                                    </li>
                                    <li><a href="goleadores.php">Goleadores Historicos</a></li>
                                    <li><a href="arbitros.php">Arbitros</a>
                                    </li>
                                    <li><a href="campeones.php">Campeones</a></li>
                                </ul>

                               </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <body style="background-color:ALICEBLUE;">



    
			 <div class="header-content">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
							<style>
					.formulariocentro {
						  margin-left:  200px;
						  width: 100%;
						  padding: 0px;  
						  position: relative;	  
						  text-align: center;

						}
					</style>
                     
                        <div class="formulariocentro" >
								
							   <form action="/jugadores.php" method="POST">
									<label for="Posicion"> Posición: </label>
									<select id="Posicion" name="Posicion">
									<option value="Portero">Portero</option>
									<option value="Defensa">Defensa</option>
									<option value="Centrocampista">Centrocampista</option>
									<option value="Delantero">Delantero</option>
									<option value="5">Todas las posiciones</option>

									</select>
									<label for="Equipo"> Equipo: </label>
									<select id="Equipo" name="Equipo">
										<option value="Real Madrid">Real Madrid </option>
										<option value="Barcelona">Barcelona</option>
										<option value="Valencia">Valencia</option>
										<option value="Sevilla">Sevilla</option>
										<option value="Real Valladolid">Real Valladolid</option>
										<option value="Leganes">Leganes</option>
										<option value="Mallorca">Mallorca</option>
										<option value="Eibar">Eibar</option>
										<option value="Celta">Celta</option>
										<option value="Villarreal">Villarreal</option>
										<option value="Granada">Granada</option>
										<option value="Getafe">Getafe</option>
										<option value="Osasuna">Osasuna</option>
										<option value="Real Sociedad">Real Sociedad</option>
										<option value="Real Betis">Real Betis</option>
										<option value="Atletico">Atletico de Madrid</option>
										<option value="Athletic">Athletic de Bilbao</option>
										<option value="Alaves">Alaves</option>
										<option value="Levante">Levante</option>
										<option value="Espanyol">Espanyol</option>
										<option value="21">Todos los equipos</option>
									</select>
									<br>
									<input type="submit" value="Buscar">
								</form>
                            <br></br>
                            
                    </div> 
                </div>
            </div> 
        </div> 
		 <div style="text-align:center;">

	

		
		  <?php 
			if(isset($_POST['Equipo']) & isset($_POST['Posicion']))
			{
		    $equipo = $_POST['Equipo'];
		    
		    $posicion = $_POST['Posicion'];
		    
		    echo'<table border="2" style="margin: 0 auto ">
		<tr >
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Edad</th>
			<th>Equipo</th>
			<th>Posicion</th>
			<th>Pais</th>
			<th>Valor del Jugador</th>
			<th width="40">Foto del jugador</th>
                </tr>';
		    
		    $tsql;
		    
		    $serverName = "proyectofp-fbh85.database.windows.net";
		    $connectionOptions = array(
		        "Database" => "proyectofp",
		        "Uid" => "funbuy",
		        "PWD" => "Microsoft123"
		    );
		    $conn = sqlsrv_connect($serverName, $connectionOptions);		    
		    $tsql1= "SELECT *  FROM [dbo].jugadores WHERE Posicion = '".$posicion."' AND Equipo = '" .$equipo. "'";
		    $tsql2="SELECT *  FROM [dbo].jugadores WHERE Posicion = '".$posicion."'";
		    $tsql3="SELECT *  FROM [dbo].jugadores WHERE Equipo = '" .$equipo. "'";
		    $tsql4= "SELECT *  FROM [dbo].jugadores where Posicion='Portero' order by equipo asc";		    
		    $tsql5= "SELECT *  FROM [dbo].jugadores where Posicion='Defensa' order by equipo asc";		    
		    $tsql6= "SELECT *  FROM [dbo].jugadores where Posicion='Centrocampista' order by equipo asc";		    
		    $tsql7= "SELECT *  FROM [dbo].jugadores where Posicion='Delantero' order by equipo asc";		
		    $tsql8= "SELECT *  FROM [dbo].jugadores where Posicion='Portero' AND Equipo = '" .$equipo. "'";
		    $tsql9= "SELECT *  FROM [dbo].jugadores where Posicion='Defensa' AND Equipo = '" .$equipo. "'";
		    $tsql10= "SELECT *  FROM [dbo].jugadores where Posicion='Centrocampista' AND Equipo = '" .$equipo. "'";
		    $tsql11= "SELECT *  FROM [dbo].jugadores where Posicion='Delantero' AND Equipo = '" .$equipo. "'";
		    
		    function getplayers($tsql, $conn){
		        
		        $getResults= sqlsrv_query($conn, $tsql);
		        
		        if ($getResults == FALSE)
		        {
		            echo (sqlsrv_errors());
		        }
		        while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC))
		        {
		            echo ("<tr>");
		            echo("<td>".$row['Nombre']."</td>". PHP_EOL);
		            echo("<td>".$row['Apellidos']."</td>". PHP_EOL);
		            echo("<td>".$row['Edad']."</td>". PHP_EOL);
		            echo("<td>".$row['Equipo']."</td>". PHP_EOL);
		            echo("<td>".$row['Posicion']."</td>". PHP_EOL);
		            echo("<td>".$row['Pais']."</td>". PHP_EOL);
		            echo("<td>".$row['Precio_Mercado'].' €'."</td>". PHP_EOL);
		            echo("<td> <img src='".$row['foto']."'></td>". PHP_EOL);
		            
		            echo ("</tr>");
		        }
		        sqlsrv_free_stmt($getResults);
		        
		    }
		    
			

		    if($equipo !=21 & $posicion != 5)
		    {
		        $tsql = $tsql1; 
		        getplayers($tsql, $conn);
		    }
		    elseif($equipo ==21 & $posicion !=5)
		    {
		        $tsql = $tsql2;
		        getplayers($tsql, $conn);
		    }
		    elseif($equipo != 21 & $posicion == 5)
		    {
		        $tsql = $tsql8;
		        getplayers($tsql, $conn);
		        $tsql = $tsql9;
		        getplayers($tsql, $conn);
		        $tsql = $tsql10;
		        getplayers($tsql, $conn);
		        $tsql = $tsql11;
		        getplayers($tsql, $conn);
		    }
		    elseif($equipo =21 & $posicion = 5)
		    {
		        $tsql = $tsql4;
		        getplayers($tsql, $conn);
		        $tsql = $tsql5;
		        getplayers($tsql, $conn);
		        $tsql = $tsql6;
		        getplayers($tsql, $conn);
		        $tsql = $tsql7;
		        getplayers($tsql, $conn);
		        
		    }
		            
			}
		 ?>
		 </div>
	</table>
    </div>
  	<br></br>


 


    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/active.js"></script>

</body>
  	<br></br>

<footer id="ft" class="abs">
    <div class="container text-center">
		<br>

      <small>La Primera Liga Española</small>
    </div>
  </footer>
</html>