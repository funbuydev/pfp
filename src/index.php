<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Liga Española</title>
	<link rel="icon" href="img/core-img/balon.png">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="preloader">
        <div class="preload-content">
            <div id="original-load"></div>
        </div>
    </div>

   

    <header class="header-area">


        <div class="top-header">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 col-sm-8">
                        <div class="breaking-news-area">
                            <div id="breakingNewsTicker" class="ticker">
                                <ul>
                                    <li><a href="#">Hello World!</a></li>
                                    <li><a href="#">Hello Universe!</a></li>
                                    <li><a href="#">Hello Original!</a></li>
                                    <li><a href="#">Hello Earth!</a></li>
                                    <li><a href="#">Hello Colorlib!</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="top-social-area">
                            <a href="https://www.facebook.com/LaLiga" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/LaLiga?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/laliga/?hl=es" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://www.linkedin.com/company/laliga" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
							<a href="https://www.youtube.com/user/laliga" data-toggle="tooltip" data-placement="bottom" title="YouTube"><i class="fa fa-youtube" aria-hidden="true"></i></a>

						</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="logo-area text-center">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <a href="index.php" class="original-logo"><img src="img/core-img/logotipo2.png" width="566" alt=""></a>
                    </div>
                </div>
            </div>
        </div>

           <div class="original-nav-area" id="stickyNav">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <nav class="classy-navbar justify-content-between">
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>
						                            <div class="navdar">

                        <div class="classy-menu" id="originalNav">
                           <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">Inicio</a></li>
                                    <li><a href="equipos.php">Equipos</a>
                                        <ul class="dropdown">
                                            <li><a href="jugadores.php">Jugadores</a></li>
                                            <li><a href="fichajes.php">Fichajes</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="partidos.php">Jugadores con mas partidos</a>
                                    </li>
                                    <li><a href="goleadores.php">Goleadores Historicos</a></li>
                                    <li><a href="arbitros.php">Arbitros</a>
                                    </li>
                                    <li><a href="campeones.php">Campeones</a></li>
                                </ul>

                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <div class="hero-area">
        <div class="hero-slides owl-carousel">
            <div class="single-hero-slide bg-img" style="background-image: url(img/bg-img/1.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-hero-slide bg-img" style="background-image: url(img/bg-img/4.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-hero-slide bg-img" style="background-image: url(img/bg-img/2.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
			<div class="single-hero-slide bg-img" style="background-image: url(img/bg-img/5.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
			<div class="single-hero-slide bg-img" style="background-image: url(img/bg-img/6.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
			<div class="single-hero-slide bg-img" style="background-image: url(img/bg-img/9.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<br></br>
	<br></br>
  


  
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/active.js"></script>

</body>
<footer id="ft" class="abs">
    <div class="container text-center">
		<br>

      <small>La Primera Liga Española</small>
    </div>
  </footer>
</html>