<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Liga Española</title>
	<link rel="icon" href="img/core-img/balon.png">
    <link rel="stylesheet" href="style.css">
	
</head>

    <div id="preloader">
        <div class="preload-content">
            <div id="original-load"></div>
        </div>
    </div>

   

    <header class="header-area" >


        <div class="top-header">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 col-sm-8">
                        <div class="breaking-news-area">
                            <div id="breakingNewsTicker" class="ticker">
                                <ul>
                                    <li><a href="#">Hello World!</a></li>
                                    <li><a href="#">Hello Universe!</a></li>
                                    <li><a href="#">Hello Original!</a></li>
                                    <li><a href="#">Hello Earth!</a></li>
                                    <li><a href="#">Hello Colorlib!</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="top-social-area">
                            <a href="https://www.facebook.com/LaLiga" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/LaLiga?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/laliga/?hl=es" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="https://www.linkedin.com/company/laliga" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
							<a href="https://www.youtube.com/user/laliga" data-toggle="tooltip" data-placement="bottom" title="YouTube"><i class="fa fa-youtube" aria-hidden="true"></i></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="logo-area text-center">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <a href="index.php" class="original-logo"><img src="img/core-img/logotipo1.png" width="566" alt=""></a>
                    </div>
                </div>
            </div>
        </div>

      <div class="original-nav-area" id="stickyNav">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <nav class="classy-navbar justify-content-between">
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>
													<div class="navdar">

                        <div class="classy-menu" id="originalNav">
                           <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <div class="classynav">

                                <ul>
                                    <li><a href="index.php">Inicio</a></li>
                                    <li><a href="equipos.php">Equipos</a>
                                        <ul class="dropdown">
                                            <li><a href="jugadores.php">Jugadores</a></li>
                                            <li><a href="fichajes.php">Fichajes</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="partidos.php">Jugadores con mas partidos</a>
                                    </li>
                                    <li><a href="goleadores.php">Goleadores Historicos</a></li>
                                    <li><a href="arbitros.php">Arbitros</a>
                                    </li>
                                    <li><a href="campeones.php">Campeones</a></li>
                                </ul>
							</div>
                                
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

   <body style="background-color:ALICEBLUE;" >
     <div style="text-align:center;">

	<TABLE  border-collapse: 'collapse' BORDER=5 CELLPADDING=10 CELLSPACING=10  bgcolor="white" style="margin: 0 auto" >
		
		 <?php 
		 $serverName = "proyectofp-fbh85.database.windows.net";
		 $connectionOptions = array(
		     "Database" => "proyectofp",
		     "Uid" => "funbuy",
		     "PWD" => "Microsoft123"
		 );
		 $conn = sqlsrv_connect($serverName, $connectionOptions);
		 $tsql= "SELECT * FROM [dbo].[arbitros]"; //update me
		 $getResults= sqlsrv_query($conn, $tsql);
		 if ($getResults == FALSE)
		 {
		     echo (sqlsrv_errors());
		 }
			$x = 0;

				 while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC))
		 {
		     if($x ==4)
		     {
		         echo ("</tr>");
		         echo ("<tr bordercolor: 'orange' >");
		         $x = 0;
		     }
		     echo("<td >".
                     "<img widht='100' src='".$row['foto']."' />".
                     "<h5>".$row['nombre']."</h5>".
		             "<p>"."Categoria: ".$row['categoria']."</br>".
		             "Comite: ".$row['comite']."</br>".
                    "</p></td>".
		         PHP_EOL);
		       
		   
								 
					//echo("<td WIDTH='50'><img onmouseover='this.style.visibility = 'hidden';"onmouseout='this.style.visibility = 'visible';'   src='".$row['foto']."' /></td>". PHP_EOL);
					// 		     echo("<td WIDTH='50'> <img src='".$row['fotoen']."'></td>". PHP_EOL);

		     $x++;
		 }
		 echo ("</tr>");
		 sqlsrv_free_stmt($getResults);
		
		 
		 
		?>
<br></br>		
		</div>
	</table>
    </div>
	<br></br>

    

  
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/active.js"></script>
<br></br>
</body>


<footer id="ft" class="abs">
    <div class="container text-center">
		<br>

      <small>La Primera Liga Española</small>
    </div>
  </footer>
</html>